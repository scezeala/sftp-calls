﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFTPCALLS
{
    class Logger
    {
        private static int LogFileSize = int.Parse("LOG_FILE_SIZE".GetKeyValue());
        private static readonly object _lock = new object();
        public static void Log(Exception ex = null, string info = null)
        {
            lock (_lock)
            {
                string logDetails = string.Empty;
                string logType = (info == null) ? "error" : "info";
                if (System.IO.File.Exists("LOG_PATH".GetKeyValue() + logType + "_log.txt"))
                {
                    System.IO.FileInfo t = new System.IO.FileInfo("LOG_PATH".GetKeyValue() + logType + "_log.txt");
                    if (t.Length > LogFileSize * 1024 * 1024)
                    {
                        t.MoveTo("LOG_PATH".GetKeyValue() + logType + "_log_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".txt");
                    }
                }
                if (ex != null)
                {
                    logDetails = $"An error occurred Exception Message : {ex.Message } with stack trace : {ex.StackTrace} and Inner Message : {ex.InnerException}";
                }
                else
                {
                    logDetails = info;
                }
                System.IO.File.AppendAllText("LOG_PATH".GetKeyValue() + logType + "_log.txt", DateTime.Now.ToString() + " " + logDetails + Environment.NewLine);
                if (info != null)
                {
                    Console.WriteLine(info);
                }
            } 
        }
    }
}
