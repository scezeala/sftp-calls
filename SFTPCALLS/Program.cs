﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFTPCALLS
{
    class Program
    {
        static void Main(string[] args)
        {
            InputParams inputParams = new InputParams(args);
            RemoteCall.getfile(inputParams.RootPath,inputParams.FileName,inputParams.DestinationPath);
        }
    }
}
