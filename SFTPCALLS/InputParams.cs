﻿using ConsoleCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFTPCALLS
{
    class InputParams : ParamsObject
    {
        public InputParams(string[] args)
            : base(args)
        {

        }

        [Switch("S")]
        public string RootPath { get; set; }
        [Switch("D")]
        public string DestinationPath { get; set; }
        [Switch("F")]
        public string FileName { get; set; }

    }


    public class FileParams
    {
        public string Hostname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Key { get; set; }
    }
}
