﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinSCP;

namespace SFTPCALLS
{
    public static class RemoteCall
    {
        public static void getfile(string source, string fileName, string destination)
        {
            var fileParams = ReadParams();
            bool output = false;
            string HostName = fileParams.Hostname;
            string UserName = fileParams.Username;
            string Password = fileParams.Password;
            string SshHostKeyFingerprint = fileParams.Key;

            try
            {
                //source = source.Replace("-", " ");
                Logger.Log(info: "file name : " + fileName);
                //fileName = fileName.Replace("+", " ");
                //var date = DateTime.Now.ToString("dd-MM-yyyy");
                // fileName = fileName.Replace("{date}", date);
                Logger.Log(info: "Connecting to FTP {" + HostName + "} to retrieve transaction files...");
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = HostName,
                    UserName = UserName,
                    Password = Password,
                    SshHostKeyFingerprint = SshHostKeyFingerprint,
                    //TlsHostCertificateFingerprint = SshHostKeyFingerprint,
                    //FtpSecure = FtpSecure.Explicit,
                    PortNumber = 22
                };

                //sessionOptions.AddRawSettings("SendBuf", "0");
                //sessionOptions.AddRawSettings("SshSimple", "0");
                using (Session session = new WinSCP.Session())
                {
                    session.Open(sessionOptions);
                    Logger.Log(info: "sftp connection successful");

                    // Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    Logger.Log(info: "About to Read Files....");

                    RemoteDirectoryInfo listDirectory = session.ListDirectory(source);

                    if (listDirectory != null && listDirectory.Files.Count() > 0)
                    {
                        string[] files = fileName.Split(',');
                        foreach (string singleFile in files)
                        {
                            Logger.Log(info: "Searching for file " + singleFile + "....");
                            var listedFiles = listDirectory.Files.Where(x => x.Name.ToUpper().Contains(singleFile.ToUpper())).ToList();
                            if (listedFiles.Any())
                            {
                               foreach(var file in listedFiles)
                                {
                                    Logger.Log(info: "File Found - " + file.FullName);
                                    Console.WriteLine("File Found - " + file.Name);
                                    transferResult = session.GetFiles(file.FullName, destination, false, transferOptions);
                                    // Throw on any error
                                    output = transferResult.IsSuccess;
                                    if (output)
                                    {
                                        Logger.Log(info: $"{file.Name} Download successfully to " + destination);
                                    }
                                    else
                                    {
                                        Logger.Log(info: $"{file.Name} Download was unsuccessfully");
                                    }

                                }
                            }
                        }


                    }

                    //foreach(RemoteFileInfo directory in listDirectory.Files.ToList())
                    //{
                    //    transferResult = session.GetFiles(directory.FullName, destination, false, transferOptions);
                    //    if (transferResult.IsSuccess)
                    //        Logger.Log(info: $"{directory.Name} Successfully downloaded");
                    //    else
                    //        Logger.Log(info: $"Unable to download {directory.Name}");
                    //}

                    //var file = listDirectory.Files.Where(x => x.Name.ToUpper().Contains(fileName.ToUpper())).FirstOrDefault();
                    //if (file != null)
                    //{
                    //    Logger.Log(info: "File Found - " + file.Name);
                    //    transferResult = session.GetFiles(source + file.Name, destination, false, transferOptions);
                    //    // Throw on any error
                    //    output = transferResult.IsSuccess;
                    //    if (output)
                    //    {
                    //        Logger.Log(info: "File Download successfully to " + destination);
                    //    }
                    //    else
                    //    {
                    //        Logger.Log(info: "File Download was unsuccessfully");
                    //    }

                    //}
                    //else
                    //{
                    //    Logger.Log(info: "No file Retrieved from FTP....");
                    //}
                    Logger.Log(info: "Connection to FTP Ends...");
                }
            }
            catch (Exception Ex)
            {
                Logger.Log(info: "Error encountered during sftp connection...Check application log for Details");
                Logger.Log(ex: Ex);
            }
            WriteOutput(output, destination);
        }


        public static void WriteOutput(bool result, string downloadPath)
        {
            if (File.Exists(downloadPath))
            {
                Logger.Log(info: "Found an existing file.......");
                Logger.Log(info: "Deleting existing file from location.........");
                File.Delete($"{downloadPath}SftpFileReader.txt");
                File.WriteAllText($"{downloadPath}SftpFileReader.txt", result.ToString());
            }
            else
            {
                File.WriteAllText($"{downloadPath}SftpFileReader.txt", result.ToString());
            }
        }

        public static FileParams ReadParams()
        {
            FileParams param = new FileParams();
            var paramsPath = AppDomain.CurrentDomain.BaseDirectory + "Param.txt";
            if (File.Exists(paramsPath))
            {
                var allLines = File.ReadLines(paramsPath).ToArray();
                param.Hostname = allLines[0].ToString();
                param.Username = allLines[1].ToString();
                param.Password = allLines[2].ToString();
                param.Key = allLines[3].ToString();
            }
            return param;
        }
    }
}
